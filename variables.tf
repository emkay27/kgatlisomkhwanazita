variable "tags" {
  type = map(any)
  description = "Map of tags for project"
  default = {
    "Name": "Kgatliso"
    "Description": "Grad-tf-assignment"
    "Department": "Graduates"
  }
}