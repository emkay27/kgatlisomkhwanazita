var AWS = require('aws-sdk');
AWS.config.update({ region: 'eu-west-1' });

const rekognition = new AWS.Rekognition({ apiVersion: '2016-06-27', region: 'eu-west-1' });
const sns = new AWS.SNS({ apiVersion: '2010-03-31' });
const dynamodb = new AWS.DynamoDB.DocumentClient();

exports.handler = (event) => {
  const s3BucketName = event.Records[0].s3.bucket.name;
  const imageKey = event.Records[0].s3.object.key;

  getDetextion(s3BucketName, imageKey);

  sendNotification(s3BucketName, imageKey);
}

let getDetextion = (s3BucketName, imageKey) => {
  let params = {
    Image: {
      S3Object: {
        Bucket: s3BucketName,
        Name: imageKey,
      }
    }
  };

  rekognition.detectText(params).promise().then((res) => {
    console.log("Rekognition output: " + JSON.stringify(res));
    updateDDB(res);
  }).catch((err) => {
    console.log("Rekognition error: " + err);
  });

}

let updateDDB = (data) => {
  var id = "id" + Math.random().toString(24).slice(4);
  var card = JSON.stringify(data);
  
  let paramsddb = {
    TableName: "ddb-mk",
    Item: {
      "user_info": id,
      "user-bbcard": card
    }
  };

  dynamodb.put(paramsddb, function (err, data) {
    if (err) console.log(err, err.stack); // error 
    else console.log("DynamoDB input: " + card);           // success
  });
}

let sendNotification = (s3BucketName, imageKey) => {
  var paramsSNS = {
    Message: ''.concat('Image uploaded to bucket.\n Bucket Name: ', s3BucketName, '\n Image Key: ', imageKey),
    TopicArn: 'arn:aws:sns:eu-west-1:296274010522:MyTopic-mk',
  };

  var publishTextPromise = sns.publish(paramsSNS).promise().then(
    (data) => {
      console.log(`Message ${paramsSNS.Message} sent to the topic ${paramsSNS.TopicArn}`);
      console.log("MessageID is " + data.MessageId);
      return data;
    }).catch((err) => {
      console.error(err, err.stack);
    });
}
