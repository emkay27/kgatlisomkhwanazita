var AWS = require('aws-sdk');
AWS.config.update({ region: 'eu-west-1' });

const dynamodb = new AWS.DynamoDB.DocumentClient();
const tableName = "ddb-mk"
exports.handler = async (event, context, callback) => {
  let params = { TableName: tableName };

  let scanResults = [];
  let items;

  do {
      items = await dynamodb.scan(params).promise();
      items.Items.forEach((item) => scanResults.push(item));
      params.ExclusiveStartKey = items.LastEvaluatedKey;
  } while (typeof items.LastEvaluatedKey != "undefined");

  callback(null, scanResults);
};
