locals {
    lambda_zip_location = "outputs1/mk.zip" // Make this to be a local variable because we use it multiple times.
}

data "archive_file" "TF-Assignment-from-CLI-mk" {
  type        = "zip"
  source_file = "TF-Assignment-from-CLI-mk.js"
  output_path = "${local.lambda_zip_location}"
}

resource "aws_lambda_function" "write_lambda" {
  filename      = "${local.lambda_zip_location}"
  function_name = "TF-Assignment-from-CLI-mk"
  role          = "${aws_iam_role.lambda_role.arn}"
  timeout       = 100
  handler       = "TF-Assignment-from-CLI-mk.handler"

  source_code_hash = "${filebase64sha256(local.lambda_zip_location)}" //This lets terraform know that there is a change in the source code so it has to be redeployed.

  runtime = "nodejs12.x"

}